package mobile

import (
	"g/x/keyboard/game"
	"github.com/hajimehoshi/ebiten/v2/mobile"
)

func init() {
	g := game.NewGame()
	mobile.SetGame(g)
}

// Dummy is a dummy exported function.
//
// gomobile doesn't compile a package that doesn't include any exported function.
// Dummy forces gomobile to compile this package.
func Dummy() {}
