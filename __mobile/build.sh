cat $0
go run \
  github.com/hajimehoshi/ebiten/v2/cmd/ebitenmobile bind  \
  -target android \
  -javapkg com.mikl.txt_simple \
  -o ./android/txt_simple/out.aar \
  -x \
  .
