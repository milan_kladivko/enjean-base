package main

/*

   # Web Builds / Production Builds

   For web builds to work correctly, we need to have to load files differently
   from a plain app that sits on a computer.

   - We can put the files next to our code and use special ways
     of reading such files from the website. The `ebiten` library
     should have a helper for doing that.
     We'll reserve this for large files that aren't needed up front,
     like assets for later levels.

   - Include the file's binary data as a part of the executable.
     With Go 1.16's `embed` package, it is fast to generate and it's
     a part of the compilation step.

   We'll use the `embed` package to load everything until the load
   times are too big. And they'll almost never be too big.


   # Development Builds and This Generator

   This mechanism is related mainly to non-development builds where assets
   are not supposed to ever change. In development, files are supposed to
   be watched for changes and reloaded whenever change to the files occurs.

   However, we'll be using this generator to grab filenames and expose
   them as constants so we never have a chance to mistype a filename.

   We can also group them into handy lists for loading by batch or type,
   fill a struct with filenames to provide structured autocompletion,
   and so on... All of that is useful in development also.

*/

import (
	"log"

	"flag"

	"os"
	"path/filepath"
)

// Program arguments
var flags = struct {
	dir         *string
	packageName *string // optional
}{
	dir: flag.String("dir", "", ""+
		"Directory to read the files from. \n"+
		"Output `.go` file will be produced at the root of that directory."),

	packageName: flag.String("pkg", "", ""+
		"Package name to use for generated files. \n"+
		"If none is provided, will use the last folder name instead."),
}

func mustParseAndValidateFlags() {
	flag.Parse()
	if *flags.dir == "" {
		log.Printf("\n\nThe `-dir` flag has to be set.\n\n")
		panic("arguments error")
	}
	*flags.dir = filepath.Clean(*flags.dir)
}

func main() {
	log.SetFlags(0)

	mustParseAndValidateFlags()

	log.Printf("Collecting file names...")
	goFiles := parseDir(*flags.dir, *flags.packageName)

	log.Printf("Writing the files...")
	var writtenFiles []string
	{
		write := func(name string, data []byte) { //closure `writtenFiles`
			err := os.WriteFile(name, data, 0644)
			if err != nil {
				panic(err)
			}
			writtenFiles = append(writtenFiles, name)
		}

		write(filepath.Join(*flags.dir, "files_dev.go"), goFiles.dev)
		write(filepath.Join(*flags.dir, "files_prod.go"), goFiles.prod)
	}
	log.Printf("Done. \nWritten files: %v.", writtenFiles)
}
