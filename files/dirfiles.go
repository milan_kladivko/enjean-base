package files

import (
	. "g/helpers"
	"io"
	"io/fs"

	"os"
	"time"
)

type (
	DirFiles struct {
		fs.FS // Passable storage object, might be an embed or real files

		// Absolute directory to the storage.
		AbsDir string
		// List of files expected to be opened.
		FileList []string

		devWatch
	}

	ChangedFile struct {
		// NOTE:  It is useful to know the filepath if the same
		//   callback is reused with multiple files.
		Filepath string
		Bytes    []byte
		DF       *DirFiles // To get adjacent files and so on...
	}
)

//

// To be used in templates that generate these filelists
// and decide what filesystem method to use.
func NewDirfiles(fs fs.FS, filelist []string) *DirFiles {
	return WithWatcher(&DirFiles{
		FS:       fs,
		AbsDir:   CallerSourceFileDir(),
		FileList: filelist,
	})
}

//

func (df *DirFiles) Open(fpath string, cb func(ChangedFile)) {
	bytes, err := df.open(fpath)
	if err != nil {
		panic(err)
	}
	cb(ChangedFile{
		Filepath: fpath,
		Bytes:    bytes,
		DF:       df,
	})
}

func (df *DirFiles) open(fpath string) ([]byte, error) {

	// NOTE:  The filepath must be relative to the filesystem,
	//   absolute paths do not work here!

	f, err := df.FS.Open(fpath)
	if err != nil {
		return nil, Err("couldn't open: %v", err)
	}
	defer f.Close()

	bytes, err := io.ReadAll(f)
	if err != nil {
		return nil, Err("couldn't read: %v", err)
	}

	return bytes, nil
}

func (df *DirFiles) Exists(fpathList ...string) bool {
	for _, fpath := range fpathList {
		fpath = df.fromabs(fpath)
		f, err := df.FS.Open(fpath)
		if err != nil {
			return false
		}
		f.Close()
	}
	return true
}

func (df *DirFiles) Modtime(fpath string) time.Time {
	abs := df.toabs(fpath)
	s, err := os.Stat(abs)
	if err != nil {
		return time.Time{} // Get treated with a zero time!
	}
	return s.ModTime()
}
