package helpers

import (
	"image"
)

// NOTE:  I am not sure if this is something I want to use for gameplay
//   physics code. For UI layout though -- XYWH definitely wins.

type Rect struct{ X, Y, W, H int }

//

// Functions for pulling out the fields as func args
func (r Rect) WH() (int, int)             { return r.W, r.H }
func (r Rect) XY() (int, int)             { return r.X, r.Y }
func (r Rect) WHXY() (int, int, int, int) { return r.W, r.H, r.X, r.Y }
func (r Rect) XYWH() (int, int, int, int) { return r.X, r.Y, r.W, r.H }

func (r Rect) WH_() (float64, float64) { return float64(r.W), float64(r.H) }
func (r Rect) XY_() (float64, float64) { return float64(r.X), float64(r.Y) }
func (r Rect) WHXY_() (float64, float64, float64, float64) {
	return float64(r.W), float64(r.H), float64(r.X), float64(r.Y)
}
func (r Rect) XYWH_() (float64, float64, float64, float64) {
	return float64(r.X), float64(r.Y), float64(r.W), float64(r.H)
}

//

// Offsetting the rect from all sides
// (>0 making the rect smaller by the same amount)
func (r Rect) WithPad(p int) Rect {
	r.X += p
	r.Y += p
	r.W -= p * 2
	r.H -= p * 2
	return r
}

// NOTE:  If you need to do a lot of cuts at once, just do some simple
//   code instead of forcing yourself into the API.
//   Reuse is not a silver bullet!

func (r Rect) CutXLeft(xCut int) (cut, rest Rect) {
	cut = r
	cut.W = xCut
	rest = r
	rest.X += xCut
	rest.W -= xCut
	return
}
func (r Rect) CutYTop(yCut int) (cut, rest Rect) {
	cut = r
	cut.H = yCut
	rest = r
	rest.Y += yCut
	rest.H -= yCut
	return
}
func (r Rect) CutXRight(xCut int) (cut, rest Rect) {
	rest = r
	rest.W -= xCut
	cut = r
	cut.W = xCut
	cut.X = r.X + r.W - xCut
	return
}
func (r Rect) CutYBottom(yCut int) (cut, rest Rect) {
	rest = r
	rest.H -= yCut
	cut = r
	cut.H = yCut
	cut.Y = r.Y + r.H - yCut
	return
}

func (r Rect) WithXY(x, y int) Rect { r.X = x; r.Y = y; return r }
func (r Rect) WithWH(w, h int) Rect { r.W = w; r.H = h; return r }

//

func (r Rect) SplitX(splits ...int) (cols []Rect) {
	for _, sp := range splits {
		if sp >= r.W {
			// Add in the rest
			cols = append(cols, r)

			// The length must be always correct.
			// If we must do more entries to satisfy the number of splits,
			// I will make them zero-thick.
			r.X += r.W
			r.W = 0
		}
		cut, rest := r.CutXLeft(sp)
		cols = append(cols, cut)
		r = rest
	}
	cols = append(cols, r)
	return
}
func (r Rect) SplitY(splits ...int) (cols []Rect) {
	for _, sp := range splits {
		if sp >= r.Y {
			// Add in the rest
			cols = append(cols, r)

			// The length must be always correct.
			// If we must do more entries to satisfy the number of splits,
			// I will make them zero-thick.
			r.Y += r.H
			r.H = 0
		}
		cut, rest := r.CutYTop(sp)
		cols = append(cols, cut)
		r = rest
	}
	cols = append(cols, r)
	return
}

//

// TODO:  Conversions for ebi rects, image rects and whatever else
//   I might need. Not really a TODO, as I will be just doing that
//   as the need comes up.

func Rect_image(from image.Rectangle) (to Rect) {
	return Rect{
		X: from.Min.X,
		Y: from.Min.Y,
		W: from.Dx(),
		H: from.Dy(),
	}
}
