package helpers

import (
	"fmt"
	"strings"
)

var (
	Fmt     = fmt.Sprintf
	Sprintf = fmt.Sprintf
)

func StringEnclosedIn(s, pre, post string) string {
	start := strings.Index(s, pre)
	if start < 0 {
		return ""
	}
	start++
	s = s[start:]

	end := strings.Index(s, post)
	if end < 0 {
		return ""
	}
	s = s[:end]

	return s
}

// getStringInBetween returns empty string if no start or end string found
// Sources:
//   https://go.dev/play/p/C2sZRYC15XN
//   https://stackoverflow.com/q/26916952
func getStringInBetween(str string, start string, end string) (result string) {
	s := strings.Index(str, start)
	if s == -1 {
		return
	}
	s += len(start)
	e := strings.Index(str[s:], end)
	if e == -1 {
		return
	}
	return str[s : s+e]
}

func CaseAll(s string, uppercaseOrLowercase bool) string {
	if uppercaseOrLowercase == true {
		return strings.ToUpper(s)
	} else {
		return strings.ToLower(s)
	}
}
