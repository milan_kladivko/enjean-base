package helpers

import (
	"time"
)

//
//  Aliases for the `time` package
//

type Time = time.Time
type Duration = time.Duration

var Now = time.Now

// In the entire project, we'll be using the same DT unit.
// Working with math is weird enough, no point in throwing in nanos
// that `time.Duration` uses. Not good for games.
// This is syntax for being able to still use `float64` for times
// interchangably, but mentioning `Secs` where appropriate is preferrable.
type Secs = float64

// To make sure everybody is at the same page about what `dt` is,
// here is the conversion function.
func DurationToSecs(dt Duration) Secs {
	// Note: Yes, this doesn't truncate the seconds. It returns the fractional
	// conversion of nanoseconds to seconds.
	return dt.Seconds()
}

//
//  Framerate metrics
//

var (
	FrameStart Time = time.Now()
	Dt         Secs = 1 // Avoid division by zero errors, if possible
)

func UpdateDt() Secs {
	now := time.Now()
	duration := now.Sub(FrameStart)

	Dt = DurationToSecs(duration)
	FrameStart = now

	return Dt
}

type perfTimer struct{ Start, End time.Time }

func PerfTimer() perfTimer            { return perfTimer{Start: time.Now()} }
func (pt *perfTimer) Stop() perfTimer { pt.End = time.Now(); return *pt }

func (pt perfTimer) Duration() time.Duration { return pt.End.Sub(pt.Start) }
func (pt perfTimer) Secs() Secs              { return DurationToSecs(pt.Duration()) }
