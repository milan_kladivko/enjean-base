package helpers

type ID uint

func (id ID) Id() ID { return id }

var incr ID = 0 // global counter of entities

func GenerateID() ID {
	incr++
	return incr
}

//

type AnyEntity interface{ Id() ID }
