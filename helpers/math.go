package helpers

import ()

func Clamp(n, min, max float64) float64 {
	if n > max {
		return max
	} else if n < min {
		return min
	} else {
		return n
	}
}
