package helpers

import (
	cconv "github.com/lucasb-eyer/go-colorful"
	std_color "image/color"
)

var BasicColor = struct {
	White, Black, Transparent Color
	Red, Green, Blue          Color
}{
	Transparent: Color{},
	Black:       Color{A: 1.0},
	White:       Color{Color: cconv.Color{R: 1, G: 1, B: 1}, A: 1},

	Red:   Hsl(350, .5, .5),
	Green: Hsl(120, .5, .5),
	Blue:  Hsl(220, .5, .5),
}

// H[0..360] S[0..1] L[0..1] A[0..1]
func Hsl(h, s, l float64) Color {
	return Color{Color: cconv.Hsl(h, s, l), A: 1.0}
}

// H[0..360] S[0..1] L[0..1] A[0..1]
func Hsla(h, s, l, a float64) Color {
	return Color{Color: cconv.Hsl(h, s, l), A: a}
}

func RandColor() Color {
	return Color{Color: cconv.HappyColor(), A: 1.0}
}

// A[0..1]
func (c Color) Opacity(a float64) Color { c.A = a; return c }

var _ std_color.Color = Color{} // @implements
type Color struct {
	// [.R .G .B] embedded.
	// By the way, the library has some neat helpers that we get
	// by embedding the type itself instead of the components.
	cconv.Color
	A float64
}

//

// That weird thing going on with those divides by alpha
// is called `alpha-premultiplication` and I don't understand
// why it's used. But that's what the `std_color.Color.RGBA()`
// returns so everybody expects that to be in there.
// Color is kinda complicated, innit?  @alpha-premult

func (c Color) RGBA() (r, g, b, a uint32) {
	// NOTE:  Copied from `go-colorful.Color.RGBA()`
	// _, _, _, _ = c.Color.RGBA()

	r = uint32((c.R*65535.0 + 0.5) / c.A)
	g = uint32((c.G*65535.0 + 0.5) / c.A)
	b = uint32((c.B*65535.0 + 0.5) / c.A)
	a = uint32(c.A*65535.0 + 0.5)
	return
}

// RGBA_01 returns the parts in a [0..1] interval for further
// multiplying.
//
// Do not use functions on our color -- there would be
// an unnecessary back-and-forth conversion included in there.
// When using our Color type, use a method instead.
func (c Color) RGBA_01_64() (r, g, b, a float64) {
	r = c.R / c.A
	g = c.G / c.A
	b = c.B / c.A // @alpha-premult
	a = c.A
	return
}
func (c Color) RGBA_01_32() (r, g, b, a float32) {
	r = float32(c.R / c.A)
	g = float32(c.G / c.A)
	b = float32(c.B / c.A) // @alpha-premult
	a = float32(c.A)
	return
}
func RGBA_01_64(c std_color.Color) (rf, gf, bf, af float64) {
	// NOTE:  Conversion copied from `ebiten.Fill(color.Color)`
	const max = 0xffff
	r, g, b, a := c.RGBA()
	rf = float64(r) / float64(a)
	gf = float64(g) / float64(a)
	bf = float64(b) / float64(a) // @alpha-premult
	af = float64(a) / max
	return
}
func RGBA_01_32(c std_color.Color) (rf, gf, bf, af float32) {
	const max = 0xffff
	r, g, b, a := c.RGBA()
	rf = float32(r) / float32(a)
	gf = float32(g) / float32(a)
	bf = float32(b) / float32(a) // @alpha-premult
	af = float32(a) / max
	return
}
