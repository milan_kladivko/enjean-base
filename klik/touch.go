package klik

import (
	. "g/helpers"
	"github.com/hajimehoshi/ebiten/v2"
)

var Touches = touchManager{}

type touchManager struct {
	All map[TouchID]*touch
}
type touch struct {
	ID TouchID
	ScreenPos
	Action
}
type TouchID ebiten.TouchID // Not really interchangable, but still net gain

//

// We keep dead touches for the timers, but only for a while.
var inactiveTouchTTL = Secs(-10)

func (tm *touchManager) Update(als *Aliases) {

	pressed := ebimapPressed()

	prev := tm.All // will delete @ **handled
	tm.All = make(map[TouchID]*touch)

	// Add in any new presses that happened this frame. Take over
	// unoccupied slots. Might be bad to do this in some cases.
	for id := range pressed {
		var prevRef *touch // pointer! important!
		prevRef, knownFromPrev := prev[id]

		if knownFromPrev {
			tm.All[id] = prevRef // preserve reference
			delete(prev, id)     // **handled
		} else {
			tm.All[id] = &touch{ID: id}
		}

		als.Add(&tm.All[id].Action, true)
	}

	for id, t := range prev { // not **handled cleanup; = not pressed anymore
		// Keep them for a while for counter reuse, even when false
		// and positions aren't even correct (will get zeroed, probably).
		if t.IsPressedTimer > inactiveTouchTTL {
			tm.All[id] = t
			als.Add(&tm.All[id].Action, false)
		}
	}
}

func (tm *touchManager) position() {
	for _, t := range tm.All {
		posx, posy := ebiTouchPos(t.ID)

		if t.IsJustPressed {
			// When we update the position, a .IsJustPressed should
			// clear the previous position to not carry any garbage
			// into a new finger-drag. Make them equal to not confuse
			// position differences.
			t.ScreenPos.PrevScreenX = posx
			t.ScreenPos.PrevScreenY = posy
		} else {
			// All clear, just do the usual thing to keep prev pos.
			t.PrevScreenX = t.ScreenX
			t.PrevScreenY = t.ScreenY
		}

		t.ScreenX = posx
		t.ScreenY = posy
	}
}

//

func ebimapPressed() map[TouchID]struct{} {
	m := map[TouchID]struct{}{}
	for _, id := range ebiten.TouchIDs() {
		m[TouchID(id)] = struct{}{}
	}
	return m
}
func ebiTouchPos(id TouchID) (x, y int) {
	return ebiten.TouchPosition(ebiten.TouchID(id))
}
