package klik

import (
	. "g/helpers"
)

type Action struct {
	IsPressed, IsJustPressed, IsJustReleased bool
	IsPressedTimer                           Secs
}

//

type ActionBind struct {
	FromRef *Action
	ToRef   *Action
}

func (als Aliases) CheckActions(binds []ActionBind) {
	for _, bind := range binds {
		als.Add(bind.ToRef, bind.FromRef.IsPressed)
	}
}

//

type Aliases map[*Action][]bool

func (to Aliases) Combine(from Aliases) {
	for aRef, list := range from {
		to.Add(aRef, list...)
	}
}
func (to Aliases) Add(aRef *Action, active ...bool) {
	to[aRef] = append(to[aRef], active...)
}

func (als Aliases) Resolve(dt Secs) {

	for a, aliases := range als {
		before := a.IsPressed
		a.IsPressed = false

		for _, pressed := range aliases {
			if pressed {
				a.IsPressed = true
			}
		}

		if a.IsPressed != before {
			*a = Action{
				IsPressed:      a.IsPressed,
				IsJustPressed:  a.IsPressed,
				IsJustReleased: !a.IsPressed,
				IsPressedTimer: 0,
			}
		} else {
			a.IsJustPressed = false
			a.IsJustReleased = false
			if a.IsPressed {
				a.IsPressedTimer += dt
			} else {
				a.IsPressedTimer -= dt
			}
		}
	}
}
