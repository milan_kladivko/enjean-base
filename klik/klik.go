package klik

import (
	. "g/helpers"
)

func UpdateControls(binds []KeyBind, dt Secs) {

	als := Aliases{}

	Touches.Update(&als)
	als.CheckMousebinds(Mouse.defaultBinds())

	als.CheckKeybinds(binds)

	als.Resolve(dt)

	// NOTE:  See these functions for details...
	//   We need the keys to resolve for touches, otherwise
	//   we won't get the correct position tracking.
	// TODO:  Totally disconnect the internal action tracking
	//   (like mouse buttons or touches) from the game-specific
	//   things and resolve sooner.
	//   This isn't good for extensibility or user-level checks.
	Touches.position()
	Mouse.position()
}
