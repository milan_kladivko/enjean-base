package assets

import (
	"bytes"
	"fmt"
	"log"
	"time"

	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/hajimehoshi/ebiten/v2/audio/mp3"
	"github.com/hajimehoshi/ebiten/v2/audio/wav"

	"jam/assets/files"
	. "jam/helpers"
)

var Sound GameSound

type (
	GameSound struct {
		Ctx  *audio.Context
		Wavs map[string]SpammableSound
	}
	SpammableSound struct {
		players []*audio.Player
	}
)

func MustLoadMP3(path string) *audio.Player {
	data := mustLoadBytes(path)
	decoded, err := mp3.Decode(Sound.Ctx, bytes.NewReader(data))
	CanPanic(err)
	player, err := audio.NewPlayer(Sound.Ctx, decoded)
	CanPanic(err)
	return player
}

func loadWav(path string) (*wav.Stream, error) {
	data := mustLoadBytes(path)
	decoded, err := wav.Decode(Sound.Ctx, bytes.NewReader(data))
	if err != nil {
		return nil, fmt.Errorf("Couldn't load file %s : %v", path, err)
	}
	return decoded, nil
}

// Get a sound player to play back
func FreeWavPlayer(soundname string) *audio.Player {
	spams, ok := Sound.Wavs[soundname]
	if !ok {
		panic(SimpleError("unknown soundname `" + soundname + "`"))
	}

	for _, p := range spams.players {
		if p.IsPlaying() == false {
			return p
		}
	}
	return nil
}
func PlayWav(soundname string) {
	plr := FreeWavPlayer(soundname)
	if plr == nil {
		// There's no free player, gotta grab one that's not done yet
		plr = Sound.Wavs[soundname].players[0]
	}
	plr.Rewind()
	plr.Play()
}

// Prepare sound for playback
func (s *GameSound) Init(logLoading bool) {

	if logLoading {
		log.Print("Importing .wav files ...")
		tstart := time.Now()
		defer func() {
			tdiff := time.Now().Sub(tstart)

			log.Printf("Done.  (loaded in %.6fs)", DurationToDt(tdiff))
		}()
	}

	s.Ctx = audio.NewContext(44100)
	s.Wavs = make(map[string]SpammableSound)

	// Importing all .wav files in the sounds folder
}
func (s *GameSound) makeMultichannelWavPlayers() {

	var wavFilepaths []string = files.GlobGroups[2]

	for _, name := range wavFilepaths {
		// Note: Player source data can't be shared between players,
		//  we have to copy the data for every player that wants the data.

		var decodedWav []byte = func(filename string) []byte {
			wavdata := mustLoadBytes(filename)
			decoded, err := wav.Decode(Sound.Ctx, bytes.NewReader(wavdata))
			CanPanic(err)
			var b bytes.Buffer
			b.ReadFrom(decoded)
			return b.Bytes()
		}(name)

		// Creating multiple readers on the same bit of data
		// TODO:  Support setting the number of players here
		spams := SpammableSound{players: make([]*audio.Player, 2)}
		for i := range spams.players {
			spams.players[i] = audio.NewPlayerFromBytes(Sound.Ctx, decodedWav)
		}
		s.Wavs[name] = spams
	}
}
