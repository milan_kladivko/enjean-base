package txt

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/text"

	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
	"golang.org/x/image/font/sfnt"

	"image/color"

	ttf "g/txt/ttf"
	"io"

	"g/helpers"
)

// TODO:
//   We will need to support multiple fonts at once to mix
//   Japanese and Latin characters into a single text draw.
//   I mean it would be possible to do without it, but it would
//   suck pretty hard to split the draw commands and stuff...
//   https://github.com/AndreKR/multiface/blob/master/multiface.go

//
// Straight aliases
//

type Face = font.Face

var CacheGlyphs = text.CacheGlyphs

// TODO:  Write a benchmark to see at what point I actually need
//   to look at using the glyph cache...

// ---------------------------------------------------------------

type DbgArgs struct {
	DestImage *ebiten.Image
	Text      string
	X, Y      int
	// Color and Face are hard-set for Dbg text, it's always
	// a white on black shadow text of size 6x12,
	// some tiny bitmap font.
}

func Dbg(a DbgArgs) {
	ebitenutil.DebugPrintAt(
		a.DestImage, a.Text, a.X, a.Y)
}

// ---------------------------------------------------------------

type DrawArgs struct {
	DestImage *ebiten.Image
	Color     color.Color

	Text string
	Face Face
	X, Y int
}

func Draw(a DrawArgs) helpers.Rect {
	text.Draw(
		a.DestImage, a.Text, a.Face, a.X, a.Y, a.Color)
	return Rect(a)
}

// Get the bounding Rect around the text that would be written.
// The Rect is affected by the X and Y, so nothing has to be
// changed at caller.
//
// NOTE:  Yes, Ebiten has `text.BoundString(Face,string)`,
//   but that does absolutely direct bound rectangle around
//   the drawn pixels or something.
//   Like a written `.` is a 4x4 Rect -- that kind of thing.
//   It's not very useful, to be honest...
// NOTE:  Non-monospace fonts might misbehave with our path.
//   Proper fonts are a bit more complicated, so we would need to either
//   copy Ebiten's code or use it instead.
func Rect(a DrawArgs) (r helpers.Rect) {
	// NOTE:  The color and destination image is ignored.

	// For printed character looping, we need to take the unicode
	// characters / runes / "code points".
	// Pretty easy in Go.
	chars := []rune(a.Text)

	// Height is always the same, regardless of the character.
	//
	// Ebiten will print breaklines in the string -- the line height then
	// is equal to the bare metrics height.
	// In that case, Ebiten cannot be configured to have a different
	// line height.
	//
	// NOTE:  If you want a function for exact bounds for a character,
	//   you can ask for it with `Face.GlyphBounds(char)`.
	//
	var (
		lineHeight = a.Face.Metrics().Height.Ceil()
		ascent     = a.Face.Metrics().Ascent.Ceil()
		capHeight  = a.Face.Metrics().CapHeight.Ceil()
	)

	r.X = a.X
	// The draw X,Y is at bottom-left of the text, so the bounding rect
	// starts at the bottom-top!
	r.Y = a.Y - lineHeight + (lineHeight - ascent)
	r.H = lineHeight + (ascent - capHeight)

	WidestWidth := 0
	{
		w := 0
		for _, char := range chars {
			if char == rune('\n') {
				r.H += lineHeight
				if w > WidestWidth {
					WidestWidth = w
				}
				w = 0
				continue
			}

			// TODO:  Ebiten might be drawing the characters with some
			//   overlap and in that case, we should do way more work.

			w_weirdint, hasGlyph := a.Face.GlyphAdvance(char) // TODO timeit
			if !hasGlyph {
				// TODO:  We should figure out what Ebiten uses for
				//   placeholder characters.
				continue
			}

			w += w_weirdint.Ceil()
		}
		if w > WidestWidth {
			WidestWidth = w
		}
	}

	r.W = WidestWidth
	return r
}

// Inserts breaklines into the `DrawArgs.Text` string and since measuring
// the font is necessary, returns the bounding Rect too.
// The return value should be the same as calling `Rect()` on
// the modified DrawArgs.
//
// TODO:  This inserts breaklines into words too. Insert it only into
//   spaces between the words -- this is crap.
//   Obviously, this requires backtracking through the text and stuff
//   so that is way worse to figure out.
//   I can do it tho :^)
func InsertBreaklinesToFitInto(fitWidth int, a *DrawArgs) (r helpers.Rect) {
	// NOTE:  For reference and docs, look at the `Rect()` func.
	//   I won't bother here...

	chars := []rune(a.Text)
	var (
		lineHeight = a.Face.Metrics().Height.Ceil()
		ascent     = a.Face.Metrics().Ascent.Ceil()
		capHeight  = a.Face.Metrics().CapHeight.Ceil()
	)
	r.X = a.X
	r.Y = a.Y - lineHeight + (lineHeight - ascent)
	r.H = lineHeight + (ascent - capHeight)

	insertAfterIndexes := make([]int, 0)
	WidestWidth := 0
	{
		w := 0
		for idx, char := range chars {
			if char == rune('\n') {
				r.H += lineHeight
				if w > WidestWidth {
					WidestWidth = w
				}
				w = 0
				continue
			}
			w_weirdint, hasGlyph := a.Face.GlyphAdvance(char)
			if !hasGlyph {
				continue
			}
			charW := w_weirdint.Ceil()

			if (w + charW) > fitWidth { // ---------- Inserting the breakline
				r.H += lineHeight
				if w > WidestWidth {
					WidestWidth = w
				}
				w = 0
				insertAfterIndexes = append(insertAfterIndexes, idx)
			} else {
				w += charW
			}
		}
		if w > WidestWidth {
			WidestWidth = w
		}
	}

	wBreaks := make([]rune, 0, len(chars)+len(insertAfterIndexes))
	{
		insLast := 0
		for _, ins := range insertAfterIndexes {
			wBreaks = append(wBreaks, chars[insLast:ins]...)
			wBreaks = append(wBreaks, '\n')
			insLast = ins
		}
		wBreaks = append(wBreaks, chars[insLast:]...)
	}
	a.Text = string(wBreaks)

	r.W = WidestWidth
	return r
}

// About taking metrics...
// Ebiten does a lot more than us. It does a lot of extra stuff
// like looking at the previous character and trying if it can
// overlap a little (Ta in non-mono fonts).
// This doesn't matter in most cases, depending on what fonts
// we choose.
func debug_textMetrics(a DrawArgs) {
	__lib := helpers.PerfTimer()
	rlib := helpers.Rect_image(text.BoundString(a.Face, a.Text))
	__lib.Stop()

	__mine := helpers.PerfTimer()
	rmine := Rect(a)
	__mine.Stop()

	helpers.Printf(
		"          `%s` -> %d:%d\nlib(%s): %s\nmine(%s): %s",
		a.Text, a.X, a.Y,
		__lib.Duration(), helpers.Doomp(rlib),
		__mine.Duration(), helpers.Doomp(rmine))
}

// ---------------------------------------------------------------

func GetFace(sizePt float64, fam *[]byte) Face {

	parsedFont := GetFont(fam)

	setting := faceSetting{size: sizePt, font: parsedFont}
	if cached, ok := faceCache[setting]; ok {
		return cached
	}

	// Standard default DPI for most screens. No need to detect it in a game,
	// it should always look the same.
	const DPI = 72

	face, err := opentype.NewFace(parsedFont, &opentype.FaceOptions{
		Size: sizePt, DPI: DPI, Hinting: font.HintingFull,
	})

	if err != nil {
		panic(err)
	}
	faceCache[setting] = face
	return face
}

func GetFont(fontDataRef *[]byte) *sfnt.Font {

	if cached, ok := fontParseCache[fontDataRef]; ok {
		return cached
	}

	parsedFont, err := opentype.Parse(*fontDataRef)

	if err != nil {
		panic(err)
	}
	fontParseCache[fontDataRef] = parsedFont
	return parsedFont
}

// ---------------------------------------------------------------

func GetFaceFile(filename string, size float64) Face {

	file, err := ttf.Files.FS.Open(filename)
	if err != nil {
		panic(err)
	}
	fontBytes, err := io.ReadAll(file)
	if err != nil {
		panic(err)
	}

	parsedFont, err := opentype.Parse(fontBytes)
	if err != nil {
		panic(err)
	}

	face, err := opentype.NewFace(parsedFont, &opentype.FaceOptions{
		Size:    size,
		DPI:     72,
		Hinting: font.HintingFull,
	})
	if err != nil {
		panic(err)
	}

	return face
}

// ---------------------------------------------------------------

var (
	fontParseCache = map[*[]byte]*sfnt.Font{}
	faceCache      = map[faceSetting]Face{}
)

type faceSetting struct {
	size float64
	font *sfnt.Font
}
