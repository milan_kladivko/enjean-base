package main

import (
	. "g/helpers"
	"g/klik"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

const (
	WIN_TITLE          = "-- TITLE GOES HERE --"
	WIN_W              = 600
	WIN_H              = 480
	DYNAMIC_RESOLUTION = true // see Layout()
)

var (
	a        = struct{ quit klik.Action }{}
	bindings = []klik.KeyBind{
		{&a.quit, ebi.KeyQ}, {&a.quit, ebi.KeyEscape},
	}
)

// ---------------------------------------------------------------

type g struct{}

func (g *g) Update() error {
	klik.UpdateControls(bindings, 0)
	if a.quit.IsJustPressed {
		return USER_EXIT
	}

	return nil
}
func (g *g) Draw(s *ebi.Image) {

}

// ---------------------------------------------------------------

func newGame() *g {
	return &g{}
}

func main() {
	ebi.SetWindowSize(WIN_W, WIN_H)
	ebi.SetWindowTitle(WIN_TITLE)
	ebi.SetWindowResizable(true)

	Fmt("Starting: [%s]", WIN_TITLE)

	if err := ebi.RunGame(newGame()); err != nil {
		if err == USER_EXIT {
			return
		}
		panic(err)
	}
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	if DYNAMIC_RESOLUTION {
		// Resolution follows window size
		return outsideW, outsideH
	} else {
		// Pixels get stretched when we resize
		return WIN_W, WIN_H
	}
}
