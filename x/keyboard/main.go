package main

import (
	. "g/helpers"
	. "g/x/keyboard/game"
	ebi "github.com/hajimehoshi/ebiten/v2"
)

func main() {
	ebi.SetWindowSize(WIN_W, WIN_H)
	ebi.SetWindowTitle(WIN_TITLE)
	ebi.SetWindowResizable(true)

	Fmt("Starting: [%s]", WIN_TITLE)

	if err := ebi.RunGame(NewGame()); err != nil {
		panic(err)
	}
}
