package game

import (
	"context"
	"fmt"
	. "g/helpers"
	"io"
	"net"
	"net/http"
	"time"
)

var (
	// We will be using this port for receiving -- both sides will
	// do its POST and GET messages against it.
	ADDR_PORT = ":8080"
	// In case a request for an IP fails, we can still show the last
	// working IP I think...
	LastMyIP = ""
)

func MyIP() string {
	// NOTE:  Doing it this way requires connecting to a out-of-LAN
	//   server. If my router fails but we are connected to the wifi,
	//   this will fail.
	// stackoverflow.com/q/23558425
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return LastMyIP
	}
	defer conn.Close()
	LastMyIP = conn.LocalAddr().(*net.UDPAddr).IP.To4().String()
	return LastMyIP
}

type theServer struct {
	// The root mux for containing endpoints
	Mux http.ServeMux
	// So we can kill it later or whatever
	HttpServer *http.Server

	// TODO:  Storage or someshit could be here, i dunno
}

func (s *theServer) AddEndpoints(g *g) {

	g.text = MyIP() + ADDR_PORT

	s.Mux.HandleFunc("/", func(resp http.ResponseWriter, q *http.Request) {

		var (
			status int    = http.StatusOK
			body   string = "OK"
			retErr error  = nil
		)
		defer func() {
			if retErr != nil {
				status = http.StatusInternalServerError
				body = Sprintf("Internal Server Error\n%v", retErr)
				Printf("ERROR: %v", retErr)
			}
			resp.WriteHeader(status)
			resp.Write([]byte(body))
		}()

		//

		switch q.Method {
		default:
			status = http.StatusMethodNotAllowed
			body = "Method Not Allowed"

		case http.MethodGet:
			body = g.text

		case http.MethodPost:
			b, err := io.ReadAll(q.Body)
			if err != nil {
				retErr = err
				return
			}
			defer q.Body.Close()

			g.text = string(b)
		}
	})
}
func (s *theServer) Run_blocking() error {

	listener, err := net.Listen("tcp", ADDR_PORT)
	if err != nil {
		return fmt.Errorf("server startup error: %v", err)
	}
	Printf("Server running on: `%s%s`", MyIP(), ADDR_PORT)
	s.HttpServer = &http.Server{
		Handler: &s.Mux,
		// Be default, Go HTTP servers have very long timeouts not fit
		// for server-to-user communication.
		ReadTimeout:  time.Second * 10,
		WriteTimeout: time.Second * 10,
	}

	err = s.HttpServer.Serve(listener)
	if err != nil {
		return fmt.Errorf("server runtime error: %v", err)
	}

	// We'll wait for ten seconds for any ongoing things to finish,
	// then just kill it by force.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	return s.HttpServer.Shutdown(ctx)
}

//

// To connect to any device on the LAN, we have to ping *every* IP
// address on the network.
// We have to go one by one whenever we want to check for other computers.
//
// A lib that does a similar thing, but it's written weird...
// https://github.com/pojntfx/invaentory
// An SO with some nice ideas...
// https://stackoverflow.com/a/60542265

func ListIPsInNetwork(cidr string) ([]string, int, error) {

	// Parse a string address to something with methods and whatnot...
	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, 0, err
	}

	// Go through all options that exist with the masked IP...
	var ips []string
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); {

		ips = append(ips, ip.String())

		// Incrementing the IP address or whatever
		for j := len(ip) - 1; j >= 0; j-- {
			ip[j]++
			if ip[j] > 0 {
				break
			}
		}
	}

	// Remove network address and broadcast address
	// NOTE:  The non-host addresses are at the start and end
	//   of the ranges -- network=0, broadcast=max.
	lenIPs := len(ips)
	if lenIPs < 2 {
		return ips, lenIPs, nil
	}
	return ips[1 : len(ips)-1], lenIPs - 2, nil
}
