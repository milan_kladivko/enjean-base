package game

import (
	. "g/helpers"
	ebi "github.com/hajimehoshi/ebiten/v2"
)

type key struct {
	Rect
	Char   string
	EbiKey ebi.Key
	KID    int
}

//

type keysOptions struct {
	Rect
}

func keys(op keysOptions) (ks []key) {

	// A key int that isn't one of the used keys.
	// It probably is *some* key, but let's hope we don't clash.
	// If we want to use something truly alright, we'd have
	// to check this number before asking ebiten for the key ID.
	const NO______ = 108

	// The keys should have some small padding.
	// Don't forget to use this with every key that gets appended
	// to the `ks` slice.
	var KeyPadding = int(2 * DPIFactor)

	var main, space Rect
	{
		h := float64(op.Rect.H)
		main, space = op.Rect.CutYTop(int(h * 3 / 4))
	}

	ks = append(ks, key{
		Char: " ", EbiKey: ebi.KeySpace,
		Rect: space.WithPad(KeyPadding),
	})

	var mainLeft, mainRight, mainCenter Rect
	{
		var center = 40.0 * DPIFactor
		side := int((float64(main.W) - center) / 2)
		cut, rest := main.CutXLeft(side)
		mainLeft = cut
		cut, rest = rest.CutXRight(side)
		mainCenter = rest
		mainRight = cut
	}

	ks = append(ks, key{
		Char: "<", EbiKey: ebi.KeyBackspace, KID: KID_backspace,
		Rect: mainCenter.WithPad(int(5 * DPIFactor)),
	})

	{ // The main key box

		// When using column stagger, multiply that stagger by this factor.
		const ExaggerateStagger = 1.6
		const rows, cols = 3.0, 10.0
		var Staggers = [cols / 2]float64{0.65, 0.6, 0.5, 0.3, 0}

		const chars = "QWERTYUIOPASDFGHJKL.ZXCVBNM..."
		var ebis = []ebi.Key{
			ebi.KeyQ, ebi.KeyW, ebi.KeyE, ebi.KeyR, ebi.KeyT,
			ebi.KeyY, ebi.KeyU, ebi.KeyI, ebi.KeyO, ebi.KeyP,
			ebi.KeyA, ebi.KeyS, ebi.KeyD, ebi.KeyF, ebi.KeyG,
			ebi.KeyH, ebi.KeyJ, ebi.KeyK, ebi.KeyL, NO______,
			ebi.KeyZ, ebi.KeyX, ebi.KeyC, ebi.KeyV, ebi.KeyB,
			ebi.KeyN, ebi.KeyM, NO______, NO______, NO______,
		}

		h := float64(mainRight.H) / rows
		w := float64(mainRight.W) / (cols / 2)

		for i := 0; i < rows*cols; i++ {
			row := i / cols
			col := i - row*cols

			symCol := col // Column, symmetric on both sides
			side := mainLeft
			if col >= cols/2 {
				side = mainRight
				col -= cols / 2
				symCol = (cols / 2) - col - 1
			}

			// Staggering the keyboard's columns around the thumb
			// NOTE:  The `int()` is necessary for maintaining paddings...
			stagger := int(h * Staggers[symCol] * ExaggerateStagger)

			ch := string([]byte{chars[i]})
			key := key{
				Char:   ch,
				EbiKey: ebis[i],
				Rect: Rect{
					W: int(w), H: int(h),
					X: side.X + int(float64(col)*w),
					Y: side.Y + int(float64(row)*h) - stagger,
				}.WithPad(KeyPadding),
			}
			ks = append(ks, key)
		}
	}

	return ks
}

//

const (
	KID_arrowLeft = iota + 1
	KID_arrowRight
	KID_arrowUp
	KID_arrowDown
	KID_backspace
)

func (k *key) doKID(g *g) {}

/* leaving it here for now...

	const chars = ".WFPBJLUYQARSTGMNEIOZXCDVKH..."
	var ebis = []ebi.Key{
		NO______, ebi.KeyW, ebi.KeyF, ebi.KeyP, ebi.KeyB,
		ebi.KeyJ, ebi.KeyL, ebi.KeyU, ebi.KeyY, ebi.KeyQ,
		ebi.KeyA, ebi.KeyR, ebi.KeyS, ebi.KeyT, ebi.KeyG,
		ebi.KeyM, ebi.KeyN, ebi.KeyE, ebi.KeyI, ebi.KeyO,
		ebi.KeyZ, ebi.KeyX, ebi.KeyC, ebi.KeyD, ebi.KeyV,
		ebi.KeyK, ebi.KeyH, NO______, NO______, NO______,
	}

/**/
