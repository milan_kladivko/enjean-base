package game

import (
	. "g/helpers"
	"g/klik"
	"g/txt"
	"image/color"

	"os"

	ebi "github.com/hajimehoshi/ebiten/v2"
	ebiutil "github.com/hajimehoshi/ebiten/v2/inpututil"
	"golang.org/x/image/font"
)

// TODO:  Vibration on all devices use the same API.
//   ( For android, a permission decl is required )
//   https://github.com/hajimehoshi/ebiten/blob/main/vibrate.go
//   We'll need to wait for the next release though...
//   https://github.com/hajimehoshi/ebiten/issues/1452

func init() { klik.SIMULATE_TOUCH_WITH_MOUSE = false }

const (
	WIN_TITLE          = "how about keyboard buttons!"
	WIN_W              = 600
	WIN_H              = 700
	DYNAMIC_RESOLUTION = true // see Layout()
)

var (
	a        = struct{ quit klik.Action }{}
	bindings = []klik.KeyBind{
		{&a.quit, ebi.KeyEscape},
	}
)

// ---------------------------------------------------------------

var _ ebi.Game = &g{} // @interface
type g struct {
	textToDump

	keys     []key
	keyProps []keyProp
}
type textToDump struct {
	screen struct{ w, h int }
	text   string
}

func NewGame() *g {
	g := g{}

	s := theServer{}
	s.AddEndpoints(&g)
	go s.Run_blocking()

	return &g
}

func (g *g) Update() error {
	dt := UpdateDt()

	klik.UpdateControls(bindings, dt)
	if a.quit.IsJustPressed {
		os.Exit(0)
	}

	g.updateHits(dt)

	return nil
}

//

// Experimenting with an interface that would abstract the mouse
// and the touch at once without undermining expresiveness.
// IMPROVEMENT:  I would prefer the most if we'd just add
//   screen positions as we would for touches.
//   Doing it from the outside like this is needlessly complicated.

type anyPointer struct {
	klik.Action
	klik.ScreenPos
	pointerType
}
type pointerType int

const (
	pMouseLeft pointerType = iota
	pMouseRight
	pMouseMiddle
	pTouch
)

func grabAllPointers() []anyPointer {
	ps := []anyPointer{
		{ScreenPos: klik.Mouse.ScreenPos, Action: klik.Mouse.ClickLeft, pointerType: pMouseLeft},
		{ScreenPos: klik.Mouse.ScreenPos, Action: klik.Mouse.ClickRight, pointerType: pMouseRight},
		{ScreenPos: klik.Mouse.ScreenPos, Action: klik.Mouse.ClickMiddle, pointerType: pMouseMiddle},
	}
	for _, t := range klik.Touches.All {
		ps = append(ps, anyPointer{ScreenPos: t.ScreenPos, Action: t.Action, pointerType: pTouch})
	}
	return ps
}
func (p anyPointer) isTouch() bool { return p.pointerType == pTouch }
func (p anyPointer) isMouse() bool { return !p.isTouch() }

//

type keyProp struct {
	sinceClick Secs
	sinceHover Secs
}

func (g *g) updateHits(dt Secs) {
	for i := range g.keyProps {
		g.keyProps[i].sinceClick += dt
		g.keyProps[i].sinceHover += dt
	}

	// Check all pointers for taps on the keyboard
	for _, p := range grabAllPointers() {
		for _, idx := range hitTest(p.ScreenX, p.ScreenY, g.keys) {
			var k = g.keys[idx]
			var props = &g.keyProps[idx]

			if p.isMouse() {
				props.sinceHover = 0
			}
			if p.IsJustPressed {
				g.keyJustClicked(k, &g.keyProps[idx])
			}
		}
	}
	// Check the hardware keyboard and hook up virtual keyboard visuals to it
	for idx, k := range g.keys {
		if ebiutil.IsKeyJustPressed(k.EbiKey) {
			g.keyJustClicked(k, &g.keyProps[idx])
		}
	}
}
func (g *g) keyJustClicked(k key, p *keyProp) {

	p.sinceClick = 0

	switch k.KID {
	default:
		g.text += caseByShift(k.Char)
	case KID_backspace:
		if len(g.text) > 0 {
			g.text = g.text[:len(g.text)-1]
		}
	}
}

func hitTest(x, y int, ks []key) (hitIndices []int) {
	for i, k := range ks {
		if x >= k.X && x <= k.X+k.W && y >= k.Y && y <= k.Y+k.H {
			hitIndices = append(hitIndices, i)
		}
	}
	return
}

// --------------------------------------------------------------

var (
	DPIFactor float64 = -1

	keyFace          font.Face = nil
	keyFaceColor               = Hsl(220, .0, .9)
	CenterX, CenterY int
)

func InitDPI(ebitenDeviceScaleFactor float64) {
	var (
		f  = ebitenDeviceScaleFactor
		fi = func(n int) int { return int(float64(n) * f) }
		ff = func(n int) float64 { return float64(n) * f }
	)
	if DPIFactor == f {
		return // So we can spam this thing as much as we need...
	}
	DPIFactor = f

	keyFace = txt.GetFace(ff(26), txt.MonoBold)
	CenterX = fi(-3)
	CenterY = fi(+7)
}

// --------------------------------------------------------------

func caseByShift(s string) string {
	return CaseAll(s, ebi.IsKeyPressed(ebi.KeyShift))
}

//

var keyBase *ebi.Image = func() *ebi.Image {
	img := ebi.NewImage(1, 1)
	img.Set(0, 0, Hsl(220, .3, .15))
	return img
}()

func stretchOverRect(r Rect, g *ebi.GeoM, padBy float64) { // util
	x, y, w, h := r.XYWH_()
	{
		// TODO:  Gotta figure out scaling around a point when doing
		//   matrix multiplications... Goddamn...
		//   That said, in that case we would still have to scale
		//   the padding and not the width!
		x -= padBy
		y -= padBy
		w += padBy * 2
		h += padBy * 2
	}
	g.Scale(w, h)
	g.Translate(x, y)
}
func (p keyProp) feedbackScale() float64 {
	scHover := 1 + (1-Clamp(p.sinceHover/Secs(.05), 0, 1))*0.2
	scClick := 1 + (1-Clamp(p.sinceClick/Secs(.12), 0, 1))*1.75
	return scHover * scClick
}

func (g *g) Draw(s *ebi.Image) {

	for i, k := range g.keys {
		sc := g.keyProps[i].feedbackScale()
		{
			var cm ebi.ColorM
			cm.Scale(sc, sc, sc, 1)
			cm.Scale(0.3, 0.3, 0.3, 1)
			var geom ebi.GeoM
			stretchOverRect(k.Rect, &geom, ((sc-1)*3*DPIFactor)+1)

			s.DrawImage(keyBase, &ebi.DrawImageOptions{GeoM: geom, ColorM: cm})
		}
		{
			var cm ebi.ColorM
			cm.Scale(sc, sc, sc, 1)
			var geom ebi.GeoM
			stretchOverRect(k.Rect.WithPad(2), &geom, ((sc-1)*4*DPIFactor)+1)

			s.DrawImage(keyBase, &ebi.DrawImageOptions{GeoM: geom, ColorM: cm})
		}
	}

	if keyFace == nil {
		return
	}

	// Drawing the legends on the keys...
	for _, k := range g.keys {

		x, y, w, h := k.XYWH()

		X := x + (w / 2) + CenterX
		Y := y + (h / 2) + CenterY

		char := caseByShift(k.Char)

		txt.Draw(txt.DrawArgs{
			DestImage: s, Face: keyFace, Color: color.Black,
			Text: char, X: X + 2, Y: Y + 2,
		})
		txt.Draw(txt.DrawArgs{
			DestImage: s, Face: keyFace, Color: keyFaceColor,
			Text: char, X: X, Y: Y,
		})
	}

	{
		var tx = []byte("     ")
		for i := 0; i < 5; i++ {
			j := len(g.text) - i - 1
			if j < 0 {
				continue
			}
			tx[len(tx)-i-1] = g.text[j]
		}
		w, h := float64(g.screen.w), float64(g.screen.h)

		txt.Draw(txt.DrawArgs{
			DestImage: s, Face: keyFace, Color: keyFaceColor,
			Text: string(tx),
			X:    int(w/2) + CenterX*len(tx),
			Y:    int((1.0 - KeyboardHPerc) * h),
		})
	}

	{ // Top data dump showing our written text...
		text := Doomp(g.textToDump)
		a := txt.DrawArgs{
			DestImage: s, Text: text, Face: keyFace,
			X: 20, Y: 40,
			Color: BasicColor.White,
		}

		a.Text = "`yŘ\nŘy" + "\nyŘ\nŘy" + "\ny\ny" + "\ny\ny" + "\ny\ny"
		debug_drawWithBackgound(a)
		a.X += 50
		a.Text = "cHH"
		debug_drawWithBackgound(a)
		a.X += 50
		a.Text = "-n\nTy"
		debug_drawWithBackgound(a)
		a.X += 50
		a.Text = "" +
			"Hello there, but will it rect correctly?\n" +
			"Is this the correct...\n" +
			"                                     ナルト 疾風伝"
		debug_drawWithBackgound(a)

		a.X = 20 // reset
		a.Y = 200
		a.Text = "This is a long paragraph of text that should have breaklines inserted into it..."
		txt.InsertBreaklinesToFitInto(140, &a)
		debug_drawWithBackgound(a)

	}
}
func debug_drawWithBackgound(a txt.DrawArgs) {
	bounding := txt.Rect(a)
	var cm ebi.ColorM
	cm.Scale(2, .2, .2, 1)
	var geom ebi.GeoM
	stretchOverRect(bounding, &geom, 5)

	a.DestImage.DrawImage(
		keyBase, &ebi.DrawImageOptions{GeoM: geom, ColorM: cm})
	txt.Draw(a)
}

// ---------------------------------------------------------------

const KeyboardHPerc = 0.30

func (g *g) refreshKeyboardLayout(w, h int) {

	var pad = 5 * DPIFactor
	hf := float64(h)
	r := Rect{X: 0, W: w,
		H: int(hf * KeyboardHPerc),
		Y: int(hf * (1 - KeyboardHPerc)),
	}.WithPad(int(pad))

	g.keys = keys(keysOptions{Rect: r})

	if len(g.keyProps) != len(g.keys) {
		g.keyProps = make([]keyProp, len(g.keys))
		for i := range g.keyProps {
			p := &g.keyProps[i]
			p.sinceClick = 9999
			p.sinceHover = 9999
		}
	}
}

func (g *g) Layout(outsideW, outsideH int) (w int, h int) {

	// On mobile (and maybe other places) the default is to render
	// at a lower resolution until we set the layout correctly.
	// We should also upscale any font sizes by this factor.
	//   https://ebiten.org/examples/highdpi.html

	var HIDPI = ebi.DeviceScaleFactor()
	InitDPI(HIDPI)
	outsideW = int(float64(outsideW) * HIDPI)
	outsideH = int(float64(outsideH) * HIDPI)
	// NOTE:  We have to ask for the DPI after the screen initializes.
	//   Loading fonts in an `init()` or `var f = func() { ... }()`
	//   will crash on mobile.
	//   Keep that in mind when working with absolute sizes of things
	//   on mobile!

	if DYNAMIC_RESOLUTION {
		if g.screen.w != outsideW || g.screen.h != outsideH {
			g.refreshKeyboardLayout(outsideW, outsideH)
		}

		// Resolution follows window size
		g.screen.w, g.screen.h = outsideW, outsideH

		return outsideW, outsideH
	} else {
		// Pixels get stretched when we resize
		return WIN_W, WIN_H
	}
}
