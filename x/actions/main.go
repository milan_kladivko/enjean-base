package main

import (
	. "g/helpers"
	. "g/klik"

	"g/txt"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"os"
)

const (
	WIN_TITLE = "Game keybindings with alternatives"
	WIN_W     = 920
	WIN_H     = 700
)

// ---------------------------------------------------------------

var a = struct {
	quit         Action
	w, a, s, d   Action
	swapDrawMode Action
}{}

var bindings = []KeyBind{
	{ActionPtr: &a.quit, Key: ebi.KeyQ},
	{&a.quit, ebi.KeyEscape},

	{&a.w, ebi.KeyW},
	{&a.w, ebi.KeyArrowUp},

	{&a.a, ebi.KeyA},
	{&a.a, ebi.KeyArrowLeft},

	{&a.s, ebi.KeyS},
	{&a.s, ebi.KeyArrowDown},

	{&a.d, ebi.KeyD},
	{&a.d, ebi.KeyArrowRight},

	{&a.swapDrawMode, ebi.KeySpace},
	{&a.swapDrawMode, ebi.KeyEnter},
}

// ---------------------------------------------------------------

type g struct{}

const (
	drawMode_Draw = iota
	drawMode_Dbg
	drawMode_LEN
)

var drawMode = drawMode_Draw

func (g) Update() error {
	dt := UpdateDt()

	UpdateControls(bindings, dt)
	if a.quit.IsJustPressed {
		os.Exit(0)
	}

	if a.swapDrawMode.IsJustPressed {
		drawMode = (drawMode + 1) % drawMode_LEN
	}

	return nil
}
func (g) Draw(s *ebi.Image) {

	spew := Doomp(bindings)

	switch drawMode {
	case drawMode_Draw:
		const size = 15
		txt.Draw(txt.DrawArgs{
			DestImage: s,
			Text:      spew,
			X:         0,
			Y:         size,
			Color:     Hsl(220, .5, .5),
			Face:      txt.GetFace(size, txt.MonoLight),
		})
	case drawMode_Dbg:
		txt.Dbg(txt.DbgArgs{
			DestImage: s,
			Text:      spew,
			X:         0,
			Y:         0,
		})
	}

	return
}

// ---------------------------------------------------------------

func main() {
	ebi.SetWindowSize(WIN_W, WIN_H)
	ebi.SetWindowTitle(WIN_TITLE)
	ebi.SetWindowResizable(true)
	if err := ebi.RunGame(g{}); err != nil {
		panic(err)
	}
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	return outsideW, outsideH
}
