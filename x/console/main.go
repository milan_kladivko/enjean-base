package main

import (
	. "g/helpers"
	"g/txt"
	"strings"

	ebi "github.com/hajimehoshi/ebiten/v2"
	//"g/klik"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

const (
	WIN_TITLE          = "Console & Editable Text Fields"
	WIN_W              = 800
	WIN_H              = 600
	DYNAMIC_RESOLUTION = true // see Layout()
)

var (
	// When we're managing written text, we can't rely much on `klik`!
	//   a = struct { quit klik.Action }{}
	//   bindings = []klik.KeyBind{
	//     {&a.quit, ebi.KeyQ}, {&a.quit, ebi.KeyEscape},
	//   }

	isPressed     = ebi.IsKeyPressed
	isJustPressed = inpututil.IsKeyJustPressed

	join = func(s ...string) string { return strings.Join(s, "") }

	clamp = func(n, min, max int) int {
		if n < min {
			return min
		} else if n > max {
			return max
		} else {
			return n
		}
	}
)

// ---------------------------------------------------------------

type g struct {
	fields []*textfield
	cursor
}

func newGame() *g {

	// Starting with just one textfield, we'll do more later
	starter := &textfield{
		x:       0,
		y:       theFontFace.Metrics().Height.Ceil() * 2,
		w:       WIN_W,
		h:       WIN_H,
		content: "Let's go man, here it is! ちょっと",
	}
	g := &g{
		fields: []*textfield{starter},
		cursor: cursor{
			focusedInto:     starter,
			stringPosOffset: 0,
			x_:              0,
			y_:              0,
		},
	}
	Printf("%s", Doomp(g))

	return g
}
func (g *g) Update() error {
	// When we're managing written text, we can't rely much on `klik`!
	// In fact, we need to completely turn it off otherwise our
	// Q or Esc key will just kill the window indiscriminately.
	if isJustPressed(ebi.KeyQ) && isPressed(ebi.KeyControl) {
		return USER_EXIT // that's a good exit, innit?
	}

	//

	// TODO:  Clicking/tapping into text

	// Changing focus
	if isJustPressed(ebi.KeyTab) {
		fs := g.fields

		var focusIndex = -1
		for i := range fs {
			if fs[i] == g.focusedInto {
				focusIndex = i
			}
		}
		if isPressed(ebi.KeyShift) {
			focusIndex--
		} else {
			focusIndex++
		}
		focusIndex %= len(fs) // Does that wrap around if negative??

		g.cursor.focusedInto = fs[focusIndex]
	}
	var focus = g.cursor.focusedInto

	// Modifying the offset by the arrow keys.
	var offset = &g.cursor.stringPosOffset
	if isJustPressed(ebi.KeyArrowLeft) {
		// TODO:  Ctrl-jumps
		*offset -= 1
	}
	if isJustPressed(ebi.KeyArrowRight) {
		*offset += 1
	}
	*offset = clamp(*offset, 0, len(focus.content))

	// Putting written characters into the text that is focused
	// at the position of offset (caret).
	if written := string(ebi.AppendInputChars(nil)); len(written) > 0 {
		var (
			pos    = *offset
			before = focus.content[:pos]
			after  = focus.content[pos:]
		)
		focus.content = join(before, written, after) // put back   ( <- )
		*offset += len(written)
	}
	// Deleting text with backspace
	if isJustPressed(ebi.KeyBackspace) {
		deletes := 1 // there will be entire words or lines, perhaps
		var (
			pos           = *offset
			partlyDeleted = focus.content[:(pos - deletes)]
			after         = focus.content[pos:]
		)
		focus.content = join(partlyDeleted, after)
		*offset -= deletes
	}

	return nil
}
func (g *g) Draw(s *ebi.Image) {
	for _, f := range g.fields {
		f.draw(s)
	}
	g.cursor.draw(s) // on top
}

// ---------------------------------------------------------------

type cursor struct {
	// Where is the pointer supposed to be pointing into right now?
	focusedInto *textfield
	// How far off should it be into the textfield's content?
	// We'll calculate what that means later using text metrics,
	// or some other means.
	stringPosOffset int

	x_, y_ float64 // we can interpolate it into place! ...Later.
}

type textfield struct {
	x, y, w, h int
	content    string
}

func (tf textfield) draw(dest *ebi.Image) {
	// Starting with really stupid dumping of the whole content.
	// No wrapping, no nothing.
	txt.Dbg(txt.DbgArgs{
		DestImage: dest,
		Text:      tf.content,
		X:         0,
		Y:         0,
	})
	txt.Draw(txt.DrawArgs{
		DestImage: dest,
		Text:      tf.content,
		X:         tf.x,
		Y:         tf.y + theFontFace.Metrics().Height.Ceil(),
		Color:     theColor,
		Face:      theFontFace,
	})
}
func (curs cursor) draw(s *ebi.Image) {
	if curs.focusedInto == nil {
		return
	}
}

//

var theFontFace = txt.GetFace(22, txt.MonoBold)
var theColor = Hsl(220, .8, .8)

// ---------------------------------------------------------------

func main() {
	ebi.SetWindowSize(WIN_W, WIN_H)
	ebi.SetWindowTitle(WIN_TITLE)
	ebi.SetWindowResizable(true)

	Fmt("Starting: [%s]", WIN_TITLE)

	if err := ebi.RunGame(newGame()); err != nil {
		if err == USER_EXIT {
			return
		}
		panic(err)
	}
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	if DYNAMIC_RESOLUTION {
		// Resolution follows window size
		return outsideW, outsideH
	} else {
		// Pixels get stretched when we resize
		return WIN_W, WIN_H
	}
}
