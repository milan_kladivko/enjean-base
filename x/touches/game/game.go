package game

import (
	. "g/helpers"
	"g/klik"
	"g/txt"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"os"
)

func init() {
	klik.SIMULATE_TOUCH_WITH_MOUSE = true
}

const (
	WIN_TITLE          = "Trying out touches for mobile"
	WIN_W              = 600
	WIN_H              = 700
	DYNAMIC_RESOLUTION = true // see Layout()
)

var (
	a        = struct{ quit klik.Action }{}
	bindings = []klik.KeyBind{
		{&a.quit, ebi.KeyQ}, {&a.quit, ebi.KeyEscape},
	}
)

// ---------------------------------------------------------------

var _ ebi.Game = &g{} // @interface
type g struct {
	textToDump
	drawText bool
	meter    FPSMeter
}
type textToDump struct {
	screen struct{ w, h int }

	textOffsets    textOffsets
	sinceLastClick Secs

	hits []hit
}

func (g *g) Update() error {
	dt := UpdateDt()
	g.meter.Snap()

	klik.UpdateControls(bindings, dt)
	if a.quit.IsJustPressed {
		os.Exit(0)
	}

	pointers := grabAllPointers()
	g.updateHits(dt, pointers)
	g.textOffsets.updateFromPointers(pointers)

	anyPressed := false
	for _, p := range pointers {
		if p.IsJustPressed {
			anyPressed = true
			break
		}
	}
	if anyPressed {
		if g.sinceLastClick < Secs(.2) {
			g.drawText = !g.drawText
		}
		g.sinceLastClick = 0
	} else {
		g.sinceLastClick += dt
	}

	return nil
}

//

// Experimenting with an interface that would abstract the mouse
// and the touch at once without undermining expresiveness.
// IMPROVEMENT:  I would prefer the most if we'd just add
//   screen positions as we would for touches.
//   Doing it from the outside like this is needlessly complicated.

type anyPointer struct {
	klik.Action
	klik.ScreenPos
	pointerType
}
type pointerType int

const (
	pMouseLeft pointerType = iota
	pMouseRight
	pMouseMiddle
	pTouch
)

func grabAllPointers() []anyPointer {
	ps := []anyPointer{
		{ScreenPos: klik.Mouse.ScreenPos, Action: klik.Mouse.ClickLeft, pointerType: pMouseLeft},
		{ScreenPos: klik.Mouse.ScreenPos, Action: klik.Mouse.ClickRight, pointerType: pMouseRight},
		{ScreenPos: klik.Mouse.ScreenPos, Action: klik.Mouse.ClickMiddle, pointerType: pMouseMiddle},
	}
	for _, t := range klik.Touches.All {
		ps = append(ps, anyPointer{ScreenPos: t.ScreenPos, Action: t.Action, pointerType: pTouch})
	}
	return ps
}
func (p anyPointer) isTouch() bool { return p.pointerType == pTouch }
func (p anyPointer) isMouse() bool { return !p.isTouch() }

//

type textOffsets struct {
	x, y   float64
	dx, dy float64
}

func (off *textOffsets) updateFromPointers(pointers []anyPointer) {

	anyWasPressed := false
	var dx, dy float64
	for _, p := range pointers {
		if p.IsPressed {
			if p.isTouch() && p.IsJustPressed {
				//continue
			}

			anyWasPressed = true
			dx = float64(p.ScreenX - p.PrevScreenX)
			dy = float64(p.ScreenY - p.PrevScreenY)
		}
	}

	if anyWasPressed {
		// Dragging here is obvious. But it might be better to ease anyway?
		off.x += dx
		off.y += dy
		// Count momentum while we are pressed -- some average over time
		// with a decreased number for old states.
		// If the number is too high, the slide seems to start too fast,
		// if too low, we could stop at the last second with our touch
		// and barely slide.
		const momentumDecay = 0.19
		off.dx = off.dx*momentumDecay + dx
		off.dy = off.dy*momentumDecay + dy
	} // no else!

	if !anyWasPressed {
		// When we are gliding out, decrease the momentum slowly.
		// The bigger the number, the longer the after-slide.
		const easingFactor = 0.85
		off.dx *= easingFactor
		off.dy *= easingFactor
		// Then add this momentum every frame to continue sliding
		// with the momentum.
		off.x += off.dx
		off.y += off.dy

	}
}

//

type hit struct {
	x, y  int // Screen-coords of the hit
	col   Color
	since Secs
}

func (g *g) updateHits(dt Secs, pointers []anyPointer) {

	withoutOld := make([]hit, 0, len(g.hits))
	const old = Secs(2)
	for i := range g.hits {
		g.hits[i].since += dt
		if g.hits[i].since < old {
			withoutOld = append(withoutOld, g.hits[i])
		}
	}
	g.hits = withoutOld

	for _, p := range pointers {
		if p.IsJustPressed {
			g.addHit(p.ScreenX, p.ScreenY)
		}
	}

}
func (g *g) addHit(x, y int) {
	g.hits = append(g.hits, hit{
		x - int(g.textOffsets.x),
		y - int(g.textOffsets.y),
		RandColor(),
		0,
	})
}

func (g *g) Draw(s *ebi.Image) {
	if g.drawText {
		g.drawDumpedControls(s)
		g.drawDumpedGameState(s)
	}
	g.drawHits(s)

	{
		w := meterKeepFrames
		h := 200
		x := g.screen.w - w - 10
		y := g.screen.h - h - 10
		g.meter.Draw(s, x, y, w, h)
	}
}
func (g *g) drawDumpedGameState(s *ebi.Image) {
	txt.Dbg(txt.DbgArgs{
		DestImage: s,
		Text:      Doomp(g.textToDump),
		X:         int(g.textOffsets.x) + 0,
		Y:         int(g.textOffsets.y) + 0,
	})
}
func (g *g) drawDumpedControls(s *ebi.Image) {
	txt.Dbg(txt.DbgArgs{
		DestImage: s,
		//Text:      Doomp(klik.Mouse) + "\n" + Doomp(klik.Touches),
		Text: Doomp(klik.Touches) + "\n" + Doomp(klik.Mouse),
		X:    int(g.textOffsets.x) + g.screen.w - 24,
		Y:    int(g.textOffsets.y) + 0,
	})
}
func (g *g) drawHits(s *ebi.Image) {
	for _, hit := range g.hits {

		var f = (hit.since / 0.5)
		const bigger = 2

		var m ebi.GeoM
		m.Translate(
			-float64(circle.Bounds().Dx())/2,
			-float64(circle.Bounds().Dy())/2)
		m.Scale(f*bigger+2, f*bigger+2)
		m.Translate(
			float64(hit.x)+g.textOffsets.x,
			float64(hit.y)+g.textOffsets.y)

		var col ebi.ColorM
		r, g, b := hit.col.R, hit.col.G, hit.col.B
		col.Scale(r, g, b, 1-(f*f*f))

		s.DrawImage(circle, &ebi.DrawImageOptions{GeoM: m, ColorM: col})
	}
}

const meterKeepFrames = 260

func NewGame() *g {
	g := g{}
	g.meter.Init(meterKeepFrames)
	return &g
}

// ---------------------------------------------------------------

func (g *g) Layout(outsideW, outsideH int) (w int, h int) {
	if DYNAMIC_RESOLUTION {
		// Resolution follows window size
		g.screen.w, g.screen.h = outsideW, outsideH
		return outsideW, outsideH
	} else {
		// Pixels get stretched when we resize
		return WIN_W, WIN_H
	}
}

//

var circle *ebi.Image = ebi.NewImageFromImage(makeCircle())
