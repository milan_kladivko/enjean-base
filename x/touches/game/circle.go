package game

import (
	"image"
	"image/color"
	"image/draw"
)

func makeCircle() (img draw.Image) {
	const r = 15
	var color = color.White
	img = image.NewRGBA(image.Rect(0, 0, 2*r, 2*r))
	drawCircle(img, r, r, r*1.0, color)
	//drawCircle(img, r, r, r*0.9, color)
	//drawCircle(img, r, r, r*0.8, color)
	//drawCircle(img, r, r, r*0.6, color)
	return img
}

func drawCircle(img draw.Image, x0, y0, r int, c color.Color) {
	x, y, dx, dy := r-1, 0, 1, 1
	err := dx - (r * 2)

	for x > y {
		img.Set(x0+x, y0+y, c)
		img.Set(x0+y, y0+x, c)
		img.Set(x0-y, y0+x, c)
		img.Set(x0-x, y0+y, c)
		img.Set(x0-x, y0-y, c)
		img.Set(x0-y, y0-x, c)
		img.Set(x0+y, y0-x, c)
		img.Set(x0+x, y0-y, c)

		if err <= 0 {
			y++
			err += dy
			dy += 2
		}
		if err > 0 {
			x--
			dx += 2
			err += dx - (r * 2)
		}
	}
}
