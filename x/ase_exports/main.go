package main

import (
	. "g/helpers"
	"g/klik"

	"os"

	"g/anim"
	"strings"

	// Whoa, that's a weird import! Remember, it cannot be relative!
	"g/x/ase_exports/files"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

// Here's the documentation for calling the `aseprite` bin
// to export instead of using the GUI.
// The options are used in the `---.export` files.
// https://www.aseprite.org/docs/cli/

// Run `go generate` in this example folder to generate the file
// handler code.
//go:generate go run ../../files/gen/ -dir files

const (
	WIN_TITLE          = "Aseprite Watch Exporting"
	WIN_W              = 320
	WIN_H              = 240
	DYNAMIC_RESOLUTION = false // see Layout()
)

var (
	a        = struct{ quit klik.Action }{}
	bindings = []klik.KeyBind{{&a.quit, ebi.KeyQ}, {&a.quit, ebi.KeyEscape}}
)

// ---------------------------------------------------------------

type g struct {
	images []*anim.Data
}

func (g *g) Update() error {
	klik.UpdateControls(bindings, 0)
	if a.quit.IsJustPressed {
		os.Exit(0)
	}

	return nil
}
func (g *g) Draw(s *ebi.Image) {

	// TODO:  Wrapping the images on the screen as much as we can
	var x float64 = 0

	// TODO:  Give each image an animated underglow for opacity recognition
	//   and to know what block corresponds to what image.

	for _, data := range g.images {
		if data == nil || data.Sheet == nil {
			continue
		}

		img := data.Sheet
		var g ebi.GeoM
		g.Translate(x, 0)
		s.DrawImage(img, &ebi.DrawImageOptions{GeoM: g})

		wid := img.Bounds().Max.X
		x += float64(wid)
	}

	return
}

// ---------------------------------------------------------------

func newGame() *g {
	var list []*anim.Data
	for _, f := range files.Files.FileList {
		if strings.HasSuffix(f, ".ase") {
			list = append(list, anim.New(files.Files, f))
		}
	}
	return &g{images: list}
}

func main() {
	ebi.SetWindowSize(WIN_W, WIN_H)
	ebi.SetWindowTitle(WIN_TITLE)
	ebi.SetWindowResizable(true)

	Fmt("Starting: [%s]", WIN_TITLE)

	if err := ebi.RunGame(newGame()); err != nil {
		panic(err)
	}
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	if DYNAMIC_RESOLUTION {
		// Resolution follows window size
		return outsideW, outsideH
	} else {
		// Pixels get stretched when we resize
		return WIN_W, WIN_H
	}
}
